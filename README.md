# Project Title
Repository per "Donky Kong", una riedizione Java del famoso Donkey Kong, platform sviluppato da Nintendo nel 1981.

## Built With

* Java Swing - Graphical User Interface
* JUnit - Testing

## Authors

* **Rispoli Luca** - *Characters' model and view features* 
* **Budini Elizabeta** - *Scores and main menu view* 
* **Samuele Gregori** - *Gameloop and barrels features* 
* **Marco Creta** - *Model and Environment features* 

## Contacts

 **Luca.rispoli@studio.unibo.it** 

## Instructions

Download and run DonkyKong.jar . 
Please make sure you have a working JRE installed.


